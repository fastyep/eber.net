﻿module Eber.Builders

open System
open System.Collections.Generic
open System.IO
open System.Text
open HtmlAgilityPack
open uTools.Funtils

let preBuilder (builder: string) (tpl: string) (file: string) =
    if String.IsNullOrEmpty builder then same else
    sprintf "%s('%s', '%s')%s" builder tpl file
    
let postBuilder (builder: string) =
    if String.IsNullOrEmpty builder then String.concat "\r\n" else
    String.concat "," >> sprintf "%s([%s])" builder

let rec buildVendor (config: Config) (vendor: VendorConfig) =
    config.LoadFiles vendor.compile
    |> Seq.map (fun file ->
        let tpl, code = 
            if Path.GetExtension file <> ".html" then null, File.ReadAllText file else
            let doc = HtmlDocument()
            doc.Load file
            doc.DocumentNode.SelectSingleNode("//template").InnerHtml
            |> Encoding.UTF8.GetBytes
            |> Convert.ToBase64String
            |> fun tpl -> tpl, doc.DocumentNode.SelectSingleNode("//script").InnerText
        if isNull vendor.import then
            Path.GetFileNameWithoutExtension file
            |> preBuilder vendor.functor tpl
            <| code
        else
            vendor.import
            |> Seq.filter (String.IsNullOrEmpty >> not)
            |> Seq.map (fun dep ->
                config.vendors
                |> Seq.find (fun a -> a.export = dep)
                |> buildVendor config )
            |> String.concat ","
            |> sprintf "%s(%s)" code)
    |> postBuilder vendor.builder
    |> (vendor.MinifyMinifier config.min)
    
let buildIndex (config: Config) =
    let index = config.LoadDoc config.index
    let head = index.DocumentNode.SelectSingleNode("//head")
    config.LoadNodes VendorConfig.PreloadNode
    |> Seq.iter head.ChildNodes.Add
    config.LoadNodes VendorConfig.CssNode
    |> Seq.iter head.ChildNodes.Add
    sprintf "<script>
Vue.template = t => c => Object.assign(c, {template: decodeURIComponent(escape(window.atob(t)))});
Vue.routes = r => r.map(function (x) {
    return Array.isArray(x.path) ? x.path.map(function (y) {
        return { path: y, component: x };
    }) : [{ path: x.path, component: x }];
}).reduce(function (r, e) {
    e.forEach(element => {
        r.push(element);
    });
    return r;
}, []);
</script>" 
    |> HtmlNode.CreateNode
    |> head.ChildNodes.Append
    config.LoadNodes VendorConfig.JsNode
    |> Seq.iter (index.DocumentNode.SelectSingleNode("//body").ChildNodes.Append)
    config.vendors
    |> Seq.tryLast
    |> Option.iter (fun main ->
        let main = config.LoadDoc main.compile
        index.DocumentNode.SelectSingleNode("//body/*[@id='app']").InnerHtml <-
            main.DocumentNode.SelectSingleNode("//template").InnerHtml)
    index