﻿namespace Eber

open System
open System.IO
open DFlex
open HtmlAgilityPack
open Microsoft.Ajax.Utilities
open uTools.Funtils

type [<CLIMutable>] Config =
    {
        root: string
        build: string
        index: string
        vendors: VendorConfig array
        
        dir: string
        min: bool
        hash: string
    }
        
    static member Load min path =        
        if File.GetAttributes(path).HasFlag FileAttributes.Directory then
            Path.Combine (path, ".conf")
        else path
        |> fun conf ->
            File.ReadAllText conf
            |> DFlexConvert.DeserializeObject<Config>
            |> fun config ->
                { config
                  with
                    root = if not min then "/" else config.root 
                    hash = if not min then null else DateTime.UtcNow.Ticks |> BitConverter.GetBytes |> toHex
                    min = min
                    dir = Path.GetDirectoryName conf }

    member config.BuildVendor (vendor: VendorConfig) =
        let file =
            Path.Combine(config.build, vendor.FileName null)
            |> FileInfo
        file.Directory.Create()
        file.FullName

    member config.LoadFiles path =
        Path.Combine(config.dir, path)
        |> Path.GetDirectoryName
        |> DirectoryInfo
        |> fun dir -> dir.EnumerateFiles(Path.GetFileName path, SearchOption.AllDirectories)
        |> Seq.map (fun file -> file.FullName)
                
    member config.LoadNodes (f: Config -> VendorConfig -> HtmlNode option) =        
        config.vendors
        |> Seq.choose (f config)
            
    member config.LoadDoc file =
        let doc = HtmlDocument()
        Path.Combine(config.dir, file)
        |> File.ReadAllText 
        |> doc.LoadHtml
        doc

and [<CLIMutable>] VendorConfig =
    {
        import: string array
        export: string
        bundle: string
        compile: string
        functor: string
        builder: string
    }
    member vendor.FileName hash =
        if isNull hash then vendor.bundle
        else vendor.bundle + "?" + hash
        
    member vendor.MinifyMinifier min =
        Path.GetExtension vendor.bundle
        |> function
            | ".css" when min -> Minifier().MinifyStyleSheet
            | ".js" when min -> Minifier().MinifyJavaScript
            | _ -> fun a -> a
            
    static member PreloadNode (config: Config) (vendor: VendorConfig) =
        Path.GetExtension vendor.bundle
        |> function
            | ".css" -> Some "style"
            | ".js" -> Some "script"
            | _ -> None
        |> Option.map(sprintf "<link href=%s%s rel=preload as=%s>" config.root (vendor.FileName config.hash)
        >> HtmlNode.CreateNode)
        
    static member CssNode (config: Config) (vendor: VendorConfig) =
        if Path.GetExtension vendor.bundle <> ".css" then None else
        vendor.FileName config.hash
        |> sprintf "<link href=%s%s rel=stylesheet>" config.root
        |> HtmlNode.CreateNode
        |> Some
        
    static member JsNode (config: Config) (vendor: VendorConfig) =
        if Path.GetExtension vendor.bundle <> ".js" then None else
        vendor.FileName config.hash
        |> sprintf "<script src=%s%s></script>" config.root
        |> HtmlNode.CreateNode
        |> Some