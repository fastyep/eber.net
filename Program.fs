﻿module Eber.App

open System
open System.IO
open DFlex
open Eber.HttpServer
open HtmlAgilityPack
open uTools.Funtils
open Eber.Builders
    
[<EntryPoint>]
let main argv =
    match Seq.tryHead argv with
    | Some "build" ->
        let config =
            argv
            |> Seq.skip 1
            |> Seq.tryHead
            |> Option.defaultValue Environment.CurrentDirectory
            |> Config.Load true
        Directory.CreateDirectory(config.build) |> ignore        
        printfn "Building index.html..."
        Path.Combine(config.build, config.index)
        |> buildIndex(config).Save
        config.vendors
        |> Seq.filter (fun a -> a.bundle <> null)
        |> Seq.iter (fun vendor ->
            printfn "Building %s" vendor.bundle
            config.BuildVendor vendor
            |> tup <| buildVendor config vendor
            |> File.WriteAllText)
        printfn "Building done."
    | Some "serve" ->
        let config =
            argv
            |> Seq.skip 1
            |> Seq.tryHead
            |> Option.defaultValue Environment.CurrentDirectory
            |> Config.Load false
        printfn "Starting development server..."
        fun path ->
            try
                config.vendors
                |> Seq.tryFind (fun a -> a.bundle = path)
                |> Option.map (fun vendor ->
                    let result = buildVendor config vendor
                    let header =
                        Path.GetExtension vendor.bundle
                        |> MimeTypes.find
                        |> sprintf "\r\nContent-Type: %s; charset=\"utf-8\""
                    fun final (response: StreamWriter) ->
                        if not final then response.Write header else
                        response.Write result
                    |> Some)
                |> Option.defaultValue (
                    fun final (response: StreamWriter) ->
                        if not final then response.Write "\r\nContent-Type: text/html; charset=\"utf-8\"" else
                        buildIndex(config).Save response
                    |> Some)
            with e ->
                printfn "%s" <| e.ToString()
                None
        |> virtualContentHandler
        |> startServer("127.0.0.1", 8080)
        printfn "Server started, press any key to stop."
        Console.ReadKey true |> ignore
    | Some a -> printfn "Command %s not found" a
    | None -> printfn "Eber v0.9.0.\nAbout: TBD..."
    0
