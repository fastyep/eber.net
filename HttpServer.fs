﻿module Eber.HttpServer

open System
open System.IO
open System.Net
open System.Net.Sockets
open HtmlAgilityPack

let acceptClient (client:TcpClient) handler = async {
   use stream = client.GetStream()
   use reader = new StreamReader(stream)
   let header = reader.ReadLine()
   if not (String.IsNullOrEmpty(header)) then
      use writer = new StreamWriter(stream)
      handler (header, writer)
      writer.Flush()
   }

let startServer (address: string, port) handler =
   let ip = IPAddress.Parse(address)
   let listener = TcpListener(ip, port)
   listener.Start() 
   async { 
      while true do 
         let! client = listener.AcceptTcpClientAsync() |> Async.AwaitTask
         acceptClient client handler |> Async.Start
   }
   |> Async.Start

type StreamWriter with
   member writer.BinaryWrite(bytes:byte[]) =
      let writer = new BinaryWriter(writer.BaseStream)
      writer.Write(bytes)

let virtualContentHandler (handler: string -> (bool -> StreamWriter -> unit) option) (header:string, response:StreamWriter) =
   let parts = header.Split ' '
   let resource = parts.[1]
   resource.Split('?').[0].Trim('/').Trim('\\').Trim().ToLower()
   |> handler
   |> function
      | Some write ->
         response.Write "HTTP/1.1 200 OK"
         write false response
         response.Write "\r\n\r\n"
         write true response
      | None -> response.Write("HTTP/1.1 404 Not found\r\n\r\n" + resource + " not found.")