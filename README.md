# Eber.NET

Real project .conf example (from Nekto a Nekomori game):

```
root="/nekto/";
build="N:\\games.nekomori.ch\\nekto";
index="index.html";
vendors: {
    bundle="css/app.css";
    compile="styles/*.css";
},{
    bundle="js/common.js";
    compile="common/*.js";
},{
    export="routes";
    compile="layouts/*.html";
    functor="Vue.template";    
    builder="Vue.routes";
},{
    import:"routes";
    bundle="js/app.js";
    compile="main.html";
};
```